<?php

require_once 'Calender.php';

function h($s)
{
    return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

$cal = new \MyApp\Calender();

?>

<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>Calender</title>
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssreset/cssreset-min.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<table>
    <thead>
    <tr>
        <th><a href="./?t=<?= h($cal->prev) ?>">&laquo;</a></th>
        <th colspan="5"><?= h($cal->yearMonth) ?></th>
        <th><a href="./?t=<?= h($cal->next) ?>">&raquo;</a></th>
    </tr>
    </thead>
    <tbody>
    <?php echo $cal->show(); ?>
    </tbody>
    <tfoot
    <tr>
        <th colspan="7"><a href="./">Today</a></th>
    </tr>
    </tfoot>
</table>
</body>
</html>
