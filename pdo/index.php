<?php
define('DB_DATABASE', 'test_db');
define('DB_USERNAME', 'dbuser');
define('DB_PASSWORD', '');
define('PDO_DSN', 'mysql:dbhost=localhost;dbname=' . DB_DATABASE);

class User {
    public $id;
}

try {
    $db = new PDO(PDO_DSN, DB_USERNAME, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    echo "inserted" . $db->lastInsertId();

    $db = null;

} catch (PDOException $e) {

    echo $e->getMessage();
}
